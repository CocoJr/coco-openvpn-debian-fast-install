#!/bin/bash

if [[ "$USER" != 'root' ]]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

if test -z "$1"
then
    echo -n "Utilisateur: "
    read user
else
    user=$1
fi

IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
if [[ "$IP" = "" ]]; then
		IP=$(wget -qO- ipv4.icanhazip.com)
fi

cd /etc/openvpn/easy-rsa/2.0/
source ./vars
export KEY_CN="$user"
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" $user
cp /usr/share/doc/openvpn*/*ample*/sample-config-files/client.conf /root/$user.ovpn
sed -i "s|remote my-server-1 1194|remote $IP 443|" /root/$user.ovpn
sed -i "/ca ca.crt/d" /root/$user.ovpn
sed -i "/cert client.crt/d" /root/$user.ovpn
sed -i "/key client.key/d" /root/$user.ovpn
echo "<ca>" >> /root/$user.ovpn
cat /etc/openvpn/easy-rsa/2.0/keys/ca.crt >> /root/$user.ovpn
echo "</ca>" >> /root/$user.ovpn
echo "<cert>" >> /root/$user.ovpn
cat /etc/openvpn/easy-rsa/2.0/keys/$user.crt >> /root/$user.ovpn
echo "</cert>" >> /root/$user.ovpn
echo "<key>" >> /root/$user.ovpn
cat /etc/openvpn/easy-rsa/2.0/keys/$user.key >> /root/$user.ovpn
echo "</key>" >> /root/$user.ovpn
echo "Le fichier de configuration du client se trouve dans '~/$user.ovpn'."
echo "Pour le récupérer: scp root@$IP:/root/$user.ovpn ~/"